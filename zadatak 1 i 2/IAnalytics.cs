﻿using System;
using System.Collections.Generic;
using System.Text;

namespace zadatak1_zadatak2
{
	interface IAnalytics
	{
		double[] CalculateAveragePerColumn(Dataset dataset);
		double[] CalculateAveragePerRow(Dataset dataset);

	}
}
