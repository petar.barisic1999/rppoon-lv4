﻿using System;
using System.Collections.Generic;
using System.Text;

namespace zadatak1_zadatak2
{
	class Adapter: IAnalytics

    {
        private Analyzer3rdParty analyticsService;
        public Adapter(Analyzer3rdParty service)
        {
            this.analyticsService = service;
        }
        public double[][] ConvertData(Dataset dataset)
        {
            IList<List<double>> Matrixlist = dataset.GetData();
            double[][] matrix = new double[Matrixlist.Count][];
            for(int i = 0; i < Matrixlist.Count; i++)
            {
                matrix[i] = new double[Matrixlist[i].Count];
            }
            for (int i = 0; i < Matrixlist.Count; i++)
            {
                for (int j = 0; j < Matrixlist[i].Count; j++)
                {
                    matrix[i][j] = Matrixlist[i][j];
                }

            }
            return matrix;

        }
        public double[] CalculateAveragePerColumn(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerColumnAverage(data);
        }
        public double[] CalculateAveragePerRow(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerRowAverage(data);
        }

        public static void ToString(double[][] matrix)
        {
            for (int i = 0; i < matrix.Length; i++)
            {
                for (int j = 0; j < matrix[i].Length; j++)
                {
                    Console.Write(matrix[i][j] + "\t");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
      }
}
