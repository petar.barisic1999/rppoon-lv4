﻿using System;

namespace zadatak1_zadatak2
{
	class Program
	{
		static void Main(string[] args)
		{
			Dataset dataset = new Dataset(@"C:\Users\perok\Desktop\CsvFile.csv");
			Adapter adapter = new Adapter(new Analyzer3rdParty());
			double[][] matrix = adapter.ConvertData(dataset);

			foreach (double element in adapter.CalculateAveragePerColumn(dataset))
			{

				Console.WriteLine(Math.Round(element,1));
			}
			Console.WriteLine("\n");

			foreach (double element in adapter.CalculateAveragePerRow(dataset))
			{
				Console.WriteLine(Math.Round(element, 1));
			}
			Console.WriteLine("\n");

			Adapter.ToString(matrix);
		}
	}
}
