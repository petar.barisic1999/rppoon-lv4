﻿using System;
using System.Collections.Generic;
using System.Text;

namespace zadatak_4
{
	class Book:IRentable
	{
		private readonly double BaseBookPrice = 3.99;
		public String BookTitle { get; private set; }
		public Book(String title) { this.BookTitle = title; }
		public string Description { get { return this.BookTitle; } }
		public double CalculatePrice() { return BaseBookPrice; }
	}
}
