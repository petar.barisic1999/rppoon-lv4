﻿using System;
using System.Collections.Generic;

namespace zadatak_4
{
	class Program
	{
		static void Main(string[] args)
		{
			List<IRentable> rentableItems = new List<IRentable>();
			rentableItems.Add(new Book("Alchemist"));
			rentableItems.Add(new Video("Pulp Fiction"));
			rentableItems.Add(new HotItem(new Book("1984")));
			rentableItems.Add(new HotItem(new Video("Fight Club")));
			RentingConsolePrinter printer = new RentingConsolePrinter();
			printer.DisplayItems(rentableItems);
			printer.PrintTotalPrice(rentableItems);
		}
	}
}
