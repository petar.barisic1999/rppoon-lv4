﻿using System;
using System.Collections.Generic;
using System.Text;

namespace zadatak_4
{
	interface IRentable
	{
		String Description { get; }
		double CalculatePrice();
	}
}
