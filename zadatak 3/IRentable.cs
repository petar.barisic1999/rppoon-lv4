﻿using System;
using System.Collections.Generic;
using System.Text;

namespace zadatak_3
{
	interface IRentable
	{
		String Description { get; }
		double CalculatePrice();

	}
}
