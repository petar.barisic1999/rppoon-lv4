﻿using System;
using System.Collections.Generic;

namespace zadatak_3
{
	class Program
	{
		static void Main(string[] args)
		{
			List<IRentable> rentableItems = new List<IRentable>();
			rentableItems.Add(new Book("Alchemist"));
			rentableItems.Add(new Video("Pulp Fiction"));
			RentingConsolePrinter printer = new RentingConsolePrinter();
			printer.DisplayItems(rentableItems);
			printer.PrintTotalPrice(rentableItems);
		}
	}
}
