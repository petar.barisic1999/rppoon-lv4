﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace zadatak_3
{
	class RentingConsolePrinter
	{
        public void PrintTotalPrice(List<IRentable> items)
        {
            Console.WriteLine("Total price: " + items.Sum(r => r.CalculatePrice()));
        }
        public void DisplayItems(List<IRentable> items)
        {
            items.ForEach(r => Console.WriteLine(r.Description));
        }
    }
}
